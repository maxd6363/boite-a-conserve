import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArchiveService {

  _archive?: Observable<Archive[]>;

  get archive() {
    return this._archive;
  }

  constructor(private _httpClient: HttpClient) {
    this._archive = this.getArchiveRandom();
	}

  getArchiveRandom(): Observable<Archive[]> {
    this._archive = this._httpClient.get<Archive[]>(`${environment.apiUrl}Archives/Random?count=10`);
    return this._archive;
  }

  findArchive(str: string): Observable<Archive[]> {
    if (str === "") {
      this._archive = this.getArchiveRandom();
    }
    else {
      this._archive = this._httpClient.get<Archive[]>(`${environment.apiUrl}Archives/Search?search=${str}&page=1&limit=30`);
    }
    return this._archive;
  }

}

export interface Archive {
  name: string,
	description: string,
	link: string,
	date: Date
}
