import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'boite-de-conserve';

  screen: string = '0';

  showScreen(newScreen: string) {
    this.screen = newScreen;
  }

}
