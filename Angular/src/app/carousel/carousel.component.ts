import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Archive, ArchiveService } from '../archive.service';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent {

  responsiveOptions: any;
  _archive?: Observable<Archive[]>;

  constructor(private _archiveService: ArchiveService) {
    this.responsiveOptions = [
        {
            breakpoint: '1024px',
            numVisible: 3,
            numScroll: 3
        },
        {
            breakpoint: '768px',
            numVisible: 2,
            numScroll: 2
        },
        {
            breakpoint: '560px',
            numVisible: 1,
            numScroll: 1
        }
    ];

    this._archive = this._archiveService.archive;
  }

}

