import { NgModule } from '@angular/core';
import { CarouselComponent } from './carousel.component';
import { CarouselModule } from 'primeng/carousel';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    CarouselComponent
  ],
  imports: [
    CarouselModule,
    BrowserAnimationsModule
  ],
  exports: [
    CarouselComponent
  ],
  providers: [],
  bootstrap: [AppCarouselModule]
})
export class AppCarouselModule { }
