import { NgModule } from '@angular/core';
import { SearchComponent } from './search.component';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    SearchComponent
  ],
  imports: [
    InputTextModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  exports: [
    SearchComponent
  ],
  providers: [],
  bootstrap: [AppSearchModule]
})
export class AppSearchModule { }
