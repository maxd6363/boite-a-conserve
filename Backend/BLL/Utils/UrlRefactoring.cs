﻿using BoiteDeConserve.DTOs;
using System.Configuration;

namespace BoiteDeConserve.BLL.Utils
{
    internal static class UrlRefactoring
    {
        internal static ICollection<ArchiveDTO> ProcessUrlImages(this ICollection<ArchiveDTO> archives)
        {
            foreach (var archiveDto in archives)
                archiveDto.ProcessUrlImages();
            return archives;
        }
        internal static ArchiveDTO ProcessUrlImages(this ArchiveDTO archive)
        {
            foreach (var image in archive.Images ?? new List<ImageDTO>())
                image.Link = GetImageUrl(image.Link);
            return archive;
        }

        internal static string GetImageUrl(string filename, bool fromBackend = true)
        {
            return fromBackend ? $"http://{ConfigurationManager.AppSettings["Backend"]}/Archives/Images/{filename}" : $"http://{ConfigurationManager.AppSettings["ImgPush"]}/{filename}";
        }
    }
}
