﻿using BoiteDeConserve.DTOs;
using Microsoft.AspNetCore.Http;

namespace BoiteDeConserve.BLL.Interfaces
{
    public interface IImageManager
    {
        public Task<ImageDTO> AddImage(ImageDTO image);
        public Task<string> UploadImage(IFormFile file);
        public Task<Stream> GetImage(string filename);
        public string GetImageUrl(string filename, bool fromBackend = true);

    }
}
