﻿using BoiteDeConserve.DTOs;

namespace BoiteDeConserve.BLL.Interfaces
{
    public interface IUserManager
    {
        public Task<ICollection<UserDTO>> GetUsers(int page, int limit);
    }
}
