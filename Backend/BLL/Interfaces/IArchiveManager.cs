﻿using BoiteDeConserve.DTOs;

namespace BoiteDeConserve.BLL.Interfaces
{
    public interface IArchiveManager
    {
        public Task<ArchiveDTO> GetArchive(int id);
        public Task<ICollection<ArchiveDTO>> GetArchives(int page, int limit);
        public Task<ICollection<ArchiveDTO>> GetArchivesRandom(int count);
        public Task<ICollection<ArchiveDTO>> GetArchivesSeach(string search, int page, int limit);
        public Task<ArchiveDTO> AddArchive(ArchiveDTO archive);
        public Task<ICollection<ArchiveDTO>> AddArchive(ICollection<ArchiveDTO> archives);
    }
}
