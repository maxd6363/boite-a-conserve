﻿using AutoMapper;
using BoiteDeConserve.BLL.Interfaces;
using BoiteDeConserve.BLL.Utils;
using BoiteDeConserve.DTOs;
using BoiteDeConserve.Entities;
using Microsoft.EntityFrameworkCore;
using static System.StringComparison;

namespace BoiteDeConserve.BLL.Implementations
{
    public class ArchiveManager : BaseManager, IArchiveManager
    {
        public ArchiveManager(IContext context, IMapper mapper) : base(context, mapper) { }

        #region Get

        public async Task<ArchiveDTO> GetArchive(int id)
        {
            var archive = await Context.Archives.FirstAsync(x => x.Id == id);
            return Mapper.Map<ArchiveDTO>(archive).ProcessUrlImages();
        }

        public async Task<ICollection<ArchiveDTO>> GetArchives(int page, int limit)
        {
            if (page < 1) page = 1;
            if (limit is < 0 or > 100) limit = 10;
            var archives = await Context.Archives.OrderBy(x => x.Id).Skip((page - 1) * limit).Take(limit).ToListAsync();
            return Mapper.Map<IList<ArchiveDTO>>(archives).ProcessUrlImages();
        }

        public async Task<ICollection<ArchiveDTO>> GetArchivesRandom(int count)
        {
            var archiveCount = await Context.Archives.CountAsync();
            if (count > archiveCount) count = archiveCount;
            var archives = await Context.Archives.ToListAsync();
            return Mapper.Map<IList<ArchiveDTO>>(archives.OrderBy(_ => Guid.NewGuid()).Take(count)).ProcessUrlImages();
        }

        public async Task<ICollection<ArchiveDTO>> GetArchivesSeach(string search, int page, int limit)
        {
            if (page < 1) page = 1;
            if (limit is < 0 or > 100) limit = 10;
            var archives = await Context.Archives.Where(x => x.FullName.Contains(search, InvariantCultureIgnoreCase) || x.Description.Contains(search, InvariantCultureIgnoreCase)).Skip((page - 1) * limit).Take(limit).ToListAsync();
            return Mapper.Map<IList<ArchiveDTO>>(archives).ProcessUrlImages();
        }

        #endregion

        #region Add

        public async Task<ArchiveDTO> AddArchive(ArchiveDTO archive)
        {
            archive.Images ??= new List<ImageDTO>();
            foreach (var archiveImage in archive.Images)
                archiveImage.ArchiveId = archive.Id;

            var archiveAdded = await Context.Archives.AddAsync(Mapper.Map<Archive>(archive));
            await Context.SaveChangesAsync();
            return Mapper.Map<ArchiveDTO>(archiveAdded.Entity);
        }

        public async Task<ICollection<ArchiveDTO>> AddArchive(ICollection<ArchiveDTO> archives)
        {
            await Context.Archives.AddRangeAsync(Mapper.Map<ICollection<Archive>>(archives));
            await Context.SaveChangesAsync();
            return archives;
        }

        #endregion

    }
}
