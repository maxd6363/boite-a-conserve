﻿using AutoMapper;
using BoiteDeConserve.BLL.Interfaces;
using BoiteDeConserve.BLL.Utils;
using BoiteDeConserve.DTOs;
using BoiteDeConserve.Entities;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Configuration;

namespace BoiteDeConserve.BLL.Implementations
{
    public class ImageManager : BaseManager, IImageManager
    {
        public ImageManager(IContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<ImageDTO> AddImage(ImageDTO image)
        {
            var entity = Mapper.Map<Image>(image);
            var imageAdded = await Context.Images.AddAsync(entity);
            await Context.SaveChangesAsync();
            return Mapper.Map<ImageDTO>(imageAdded.Entity);
        }

        public async Task<string> UploadImage(IFormFile file)
        {
            var stream = new MemoryStream();
            using var multipartFormContent = new MultipartFormDataContent();
            await file.CopyToAsync(stream);
            using var httpClient = new HttpClient();
            using var request = new HttpRequestMessage(new HttpMethod("POST"), $"http://{ConfigurationManager.AppSettings["ImgPush"]}");
            var multipartContent = new MultipartFormDataContent { { new ByteArrayContent(stream.ToArray()), "file", file.FileName } };
            request.Content = multipartContent;
            var response = await httpClient.SendAsync(request);
            return JsonConvert.DeserializeObject<ImgPushResponse>(await response.Content.ReadAsStringAsync())?.Filename ?? string.Empty;
        }

        public async Task<Stream> GetImage(string filename)
        {
            var client = new HttpClient();
            var result = await client.GetAsync(GetImageUrl(filename, false));
            return await result.Content.ReadAsStreamAsync();
        }

        public string GetImageUrl(string filename, bool fromBackend = true)
        {
            return UrlRefactoring.GetImageUrl(filename, fromBackend);
        }

    }
}
