﻿using AutoMapper;
using BoiteDeConserve.BLL.Interfaces;
using BoiteDeConserve.DTOs;
using BoiteDeConserve.Entities;
using Microsoft.EntityFrameworkCore;

namespace BoiteDeConserve.BLL.Implementations
{
    public class UserManager : BaseManager, IUserManager
    {
        public UserManager(IContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<ICollection<UserDTO>> GetUsers(int page, int limit)
        {
            if (page < 1) page = 1;
            if (limit is < 0 or > 100) limit = 10;
            var users = await Context.Users.Skip((page - 1) * limit).Take(limit).ToListAsync();
            return Mapper.Map<IList<UserDTO>>(users);
        }

    }
}
