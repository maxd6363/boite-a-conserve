﻿using AutoMapper;
using BoiteDeConserve.Entities;

namespace BoiteDeConserve.BLL
{
    public class BaseManager : IBaseManager
    {
        protected IContext Context { get; }
        protected IMapper Mapper { get; }

        public BaseManager(IContext context, IMapper mapper)
        {
            Context = context;
            Mapper = mapper;
        }
    }
}
