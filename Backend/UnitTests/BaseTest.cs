﻿using AutoMapper;
using AutoMapper.EquivalencyExpression;
using BoiteDeConserve.DTOs.Profiles;
using BoiteDeConserve.Entities;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace BoiteDeConserve.UnitTests
{
    internal class BaseTest
    {
        protected IContext Context { get; private set; }
        protected IMapper Mapper { get; private set; }

        [SetUp]
        public void Setup()
        {
            Context = new Context(
                new DbContextOptionsBuilder<Context>()
                    .UseLazyLoadingProxies()
                    .UseMySQL("Server=82.66.59.41;Port=40101;Database=boitedeconserve;Uid=dev;Pwd=dev;")
                    .Options);

            var expression = new MapperConfigurationExpression();
            expression.AddProfile<UserMap>();
            expression.AddProfile<FiliereMap>();
            expression.AddProfile<ArchiveMap>();
            expression.AddProfile<ImageMap>();
            expression.AddCollectionMappers();

            Mapper = new Mapper(new MapperConfiguration(expression));
        }
    }
}