﻿using BoiteDeConserve.BLL.Implementations;
using BoiteDeConserve.BLL.Interfaces;
using NUnit.Framework;
using static NUnit.Framework.Assert;

namespace BoiteDeConserve.UnitTests.BLL
{
    internal class ArchiveUnitTests : BaseTest
    {
        private IArchiveManager ArchiveManager { get; set; } = null!;

        [SetUp]
        public void SetupArchive()
        {
            ArchiveManager = new ArchiveManager(Context, Mapper);
        }

        [Test]
        public async Task GetArchive()
        {
            var archive = await ArchiveManager.GetArchive(1);

            That(archive, Is.Not.Null);
            That(archive.FullName, Is.EqualTo("ArchiveTU1"));
            That(archive.Description, Is.EqualTo("ArchiveDescriptionTU"));
            That(archive.Images, Is.Not.Null);
            That(archive.Images?.Count ?? 0, Is.EqualTo(10));
        }

        [Test]
        public async Task GetArchives()
        {
            var archives = await ArchiveManager.GetArchives(1, 5);

            That(archives, Is.Not.Null);
            That(archives.Count, Is.EqualTo(5));
        }

        [Test]
        public async Task GetArchivesSearch()
        {
            var archives = await ArchiveManager.GetArchivesSeach("ArchiveTU1", 1, 10);

            That(archives, Is.Not.Null);
            That(archives.Count, Is.EqualTo(2));
        }
    }
}