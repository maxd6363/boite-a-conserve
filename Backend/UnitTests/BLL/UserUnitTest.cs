﻿using BoiteDeConserve.BLL.Implementations;
using BoiteDeConserve.BLL.Interfaces;
using NUnit.Framework;
using static NUnit.Framework.Assert;

namespace BoiteDeConserve.UnitTests.BLL
{
    internal class UserUnitTest : BaseTest
    {
        private IUserManager UserManager { get; set; } = null!;

        [SetUp]
        public void SetupUser()
        {
            UserManager = new UserManager(Context, Mapper);
        }

        [Test]
        public async Task GetUsers()
        {
            var users = await UserManager.GetUsers(1,100);
            That(users, Is.Not.Null);
            That(users.Count, Is.EqualTo(1));
            That(users.First()?.FirstName, Is.EqualTo("MichelTU"));
            That(users.First()?.Name, Is.EqualTo("LaplaceTU"));
        }

    }
}
