﻿namespace BoiteDeConserve.DTOs
{
    public class ImageDTO : IBaseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public int ArchiveId { get; set; }
    }
}
