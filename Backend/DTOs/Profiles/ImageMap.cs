﻿using AutoMapper;
using BoiteDeConserve.Entities;

namespace BoiteDeConserve.DTOs.Profiles
{
    public class ImageMap : Profile
    {
        public ImageMap()
        {
            CreateMap<Image, ImageDTO>();
            CreateMap<ImageDTO, Image>();
        }
    }
}
