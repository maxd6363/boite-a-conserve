﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace BoiteDeConserve.Entities
{
    [Table("image")]
    [Index(nameof(ArchiveId), Name = "archive_id")]
    public partial class Image
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        [StringLength(256)]
        public string Name { get; set; }
        [Column("description")]
        [StringLength(256)]
        public string Description { get; set; }
        [Column("link")]
        [StringLength(256)]
        public string Link { get; set; }
        [Column("archive_id")]
        public int ArchiveId { get; set; }

        [ForeignKey(nameof(ArchiveId))]
        [InverseProperty("Images")]
        public virtual Archive Archive { get; set; }
    }
}
