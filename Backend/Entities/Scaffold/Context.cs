﻿using Microsoft.EntityFrameworkCore;
using System.Configuration;

#nullable disable

namespace BoiteDeConserve.Entities
{
    public partial class Context : DbContext
    {
        public Context()
        {
        }

        public Context(DbContextOptions<Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Archive> Archives { get; set; }
        public virtual DbSet<Filiere> Filieres { get; set; }
        public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseLazyLoadingProxies();
                optionsBuilder.UseMySQL(ConfigurationManager.AppSettings["Database"]);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Archive>(entity =>
            {
                entity.HasOne(d => d.Owner)
                    .WithMany(p => p.Archives)
                    .HasForeignKey(d => d.OwnerId)
                    .HasConstraintName("archive_ibfk_1");
            });

            modelBuilder.Entity<Filiere>(entity =>
            {
                entity.HasOne(d => d.Responsable)
                    .WithMany(p => p.Filieres)
                    .HasForeignKey(d => d.ResponsableId)
                    .HasConstraintName("filiere_responsable_id_fk");
            });

            modelBuilder.Entity<Image>(entity =>
            {
                entity.HasOne(d => d.Archive)
                    .WithMany(p => p.Images)
                    .HasForeignKey(d => d.ArchiveId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("image_ibfk_1");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasOne(d => d.FiliereNavigation)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.Filiere)
                    .HasConstraintName("user_ibfk_1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
