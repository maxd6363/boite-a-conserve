﻿namespace BoiteDeConserve.Entities
{
    public partial class Context : IContext
    {
        public async Task<int> SaveChangesAsync()
        {
            return await SaveChangesAsync(CancellationToken.None);
        }
    }
}
