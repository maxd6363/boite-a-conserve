﻿using BoiteDeConserve.BLL.Interfaces;
using BoiteDeConserve.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace BoiteDeConserve.API.Controllers
{

    [ApiController]
    public class UserController : BaseController
    {
        private IUserManager UserManager { get; }

        public UserController(IUserManager userManager)
        {
            UserManager = userManager;
        }

        [HttpGet]
        [Route("[controller]s")]
        [ProducesResponseType(statusCode: 200, type: typeof(ICollection<UserDTO>))]
        public async Task<IActionResult> Get(int page = 1, int limit = 100)
        {
            try
            {
                var users = await UserManager.GetUsers(page, limit);
                return Ok(users);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }


    }
}
