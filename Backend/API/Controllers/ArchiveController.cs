﻿using BoiteDeConserve.BLL.Interfaces;
using BoiteDeConserve.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace BoiteDeConserve.API.Controllers
{
    [ApiController]
    public class ArchiveController : BaseController
    {
        private IArchiveManager ArchiveManager { get; }

        public ArchiveController(IArchiveManager archiveManager)
        {
            ArchiveManager = archiveManager;
        }

        #region ARCHIVE_GET

        [HttpGet]
        [Route("[controller]/{id}")]
        [ProducesResponseType(statusCode: 200, type: typeof(ArchiveDTO))]
        public async Task<IActionResult> GetArchive(int id)
        {
            try
            {
                var archive = await ArchiveManager.GetArchive(id);
                return Ok(archive);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        [HttpGet]
        [Route("[controller]s")]
        [ProducesResponseType(statusCode: 200, type: typeof(ICollection<ArchiveDTO>))]
        public async Task<IActionResult> GetArchives(int page = 1, int limit = 10)
        {
            try
            {
                var archives = await ArchiveManager.GetArchives(page, limit);
                return archives.Count == 0 ? NotFound(archives) : Ok(archives);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        [HttpGet]
        [Route("[controller]s/Random")]
        [ProducesResponseType(statusCode: 200, type: typeof(ICollection<ArchiveDTO>))]
        public async Task<IActionResult> GetArchivesRandom(int count = 10)
        {
            try
            {
                var archives = await ArchiveManager.GetArchivesRandom(count);
                return archives.Count == 0 ? NotFound(archives) : Ok(archives);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        [HttpGet]
        [Route("[controller]s/Search")]
        [ProducesResponseType(statusCode: 200, type: typeof(ICollection<ArchiveDTO>))]
        public async Task<IActionResult> GetArchivesSearch(string search, int page = 1, int limit = 10)
        {
            try
            {
                var archives = await ArchiveManager.GetArchivesSeach(search, page, limit);
                return archives.Count == 0 ? NotFound(archives) : Ok(archives);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        #endregion

        #region ARCHIVE_POST

        [HttpPost]
        [Route("[controller]")]
        [ProducesResponseType(statusCode: 200, type: typeof(ArchiveDTO))]
        public async Task<IActionResult> Post([FromBody] ArchiveDTO archiveDto)
        {
            try
            {
                var archive = await ArchiveManager.AddArchive(archiveDto);
                return Ok(archive);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        #endregion
    }
}
