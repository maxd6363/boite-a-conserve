using AutoMapper.EquivalencyExpression;
using BoiteDeConserve.BLL.Implementations;
using BoiteDeConserve.BLL.Interfaces;
using BoiteDeConserve.DTOs.Profiles;
using BoiteDeConserve.Entities;
using System.Reflection;
using Microsoft.AspNetCore.RateLimiting;
using Prometheus;
using Serilog;
using Serilog.Exceptions;
using Serilog.Sinks.Elasticsearch;

var builder = WebApplication.CreateBuilder(args);


ConfigureLogging();
builder.Host.UseSerilog();

builder.Services.AddControllers();
builder.Services.AddTransient<IContext, Context>();
builder.Services.AddScoped<IArchiveManager, ArchiveManager>();
builder.Services.AddScoped<IUserManager, UserManager>();
builder.Services.AddScoped<IImageManager, ImageManager>();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddRateLimiter(o =>
    o.AddFixedWindowLimiter("Fixed", options =>
    {
        options.PermitLimit = 4;
        options.Window = TimeSpan.FromMinutes(1);
    }));


builder.Services.AddAutoMapper((_, am) =>
{
    am.AddProfile<UserMap>();
    am.AddProfile<FiliereMap>();
    am.AddProfile<ArchiveMap>();
    am.AddProfile<ImageMap>();
    am.AddCollectionMappers();
}, Assembly.GetAssembly(typeof(Program)));


var app = builder.Build();

app.UseRouting();
app.UseRateLimiter();
app.UseHttpMetrics();
app.UseCors(x => x.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
app.UseSwagger();
app.UseSwaggerUI();
app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.MapMetrics();
app.Run();



void ConfigureLogging()
{
    var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? string.Empty;
    var configuration = new ConfigurationBuilder()
        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
        .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: true)
        .Build();

    Log.Logger = new LoggerConfiguration()
        .Enrich.FromLogContext()
        .Enrich.WithExceptionDetails()
        .WriteTo.Debug()
        .WriteTo.Console()
        .WriteTo.Elasticsearch(ConfigureElasticSink(configuration, environment))
        .Enrich.WithProperty("Environment", environment)
        .ReadFrom.Configuration(configuration)
        .CreateLogger();
}

ElasticsearchSinkOptions ConfigureElasticSink(IConfigurationRoot configuration, string environment)
{
    return new ElasticsearchSinkOptions(new Uri(configuration["ElasticConfiguration:Uri"]))
    {
        AutoRegisterTemplate = true,
        IndexFormat = $"{Assembly.GetExecutingAssembly().GetName().Name?.ToLower().Replace(".", "-")}-{environment?.ToLower().Replace(".", "-")}-{DateTime.UtcNow:yyyy-MM}"
    };
}