import os

import discord
from discord.ext import commands
from dotenv import load_dotenv


load_dotenv(dotenv_path=".env")

default_intents = discord.Intents.default()
default_intents.members = True  # Vous devez activer les intents dans les paramètres du Bot
bot = commands.Bot(command_prefix="!", intents=default_intents)


@bot.event
async def on_ready():
    print("Le bot est connecté.")


@bot.event
async def on_member_join(member):
    print(f"{member.display_name} rendez-vous dans le channel iddentité ;) ")


@bot.command(name="add_archive")
async def add_archive(ctx):
    attachment_url = ctx.message.attachments[0].url
    file_request = requests.get(attachment_url)
    print(file_request.content)
    # send file to C#


bot.run(os.getenv("TOKEN"))