cd ..

# Database

cd Database
docker build -t boite-de-conserve-db-prod:latest .
cd ..

# Backend

cd Backend
docker build -t boite-de-conserve-backend-prod:latest -f ./API/Dockerfile .
cd ..

# Frontend
cd Angular
docker build -t boite-de-conserve-frontend-prod:latest .
cd ..

