-- CREATING TABLES --

create table filiere (
    id integer not null AUTO_INCREMENT,
    name varchar(256),
    responsable_id integer,
    primary key(id)
);

create table user (
    id integer not null AUTO_INCREMENT,
    name varchar(256),
    first_name varchar(256),
    promotion integer,
    filiere integer,
    city_origin varchar(256),
    country varchar(256),
    primary key(id),
    foreign key(filiere) references filiere(id)
);

create table archive (
    id integer not null AUTO_INCREMENT,
    full_name varchar(256),
    description varchar(256),
    date date,
    owner_id integer,
    primary key(id),
    foreign key (owner_id) references user(id)
);

create table image(
    id integer not null AUTO_INCREMENT,
    name varchar(256),
    description varchar(256),
    link varchar(256),
    archive_id integer not null,
    primary key(id),
    foreign key(archive_id) references archive(id)
);

-- ADDING FK WITH ALTER TABLE TO AVOID CIRCULAR FK --
alter table filiere add constraint filiere_responsable_id_fk foreign key (responsable_id) references user(id);

-- INSERTING DATA --
insert into user(name, first_name) values("Laplace", "Michel");

insert into filiere(name) values("F1");
insert into filiere(name) values("F2");
insert into filiere(name) values("F3");
insert into filiere(name) values("F4");
insert into filiere(name) values("F5");

insert into archive(full_name, description) values("ArchiveTest", "ArchiveTestDescs");

insert into image(name, description, link, archive_id) values("Test", "Test desc", "https://i.ibb.co/qpZFSV8/image-2022-10-25-161553349.png", 1);

-- COMMITING --
commit;