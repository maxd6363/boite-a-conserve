-- DELETING DATA --
delete from filiere;
delete from user;
delete from image;
delete from archive;

ALTER TABLE filiere AUTO_INCREMENT = 1;
ALTER TABLE user AUTO_INCREMENT = 1;
ALTER TABLE image AUTO_INCREMENT = 1;
ALTER TABLE archive AUTO_INCREMENT = 1;


-- INSERTING DATA --
insert into user(name, first_name) values("LaplaceTU", "MichelTU");

insert into filiere(name) values("F1");
insert into filiere(name) values("F2");
insert into filiere(name) values("F3");
insert into filiere(name) values("F4");
insert into filiere(name) values("F5");

insert into archive(full_name, description) values("ArchiveTU1", "ArchiveDescriptionTU");
insert into archive(full_name, description) values("ArchiveTU2", "ArchiveDescriptionTU");
insert into archive(full_name, description) values("ArchiveTU3", "ArchiveDescriptionTU");
insert into archive(full_name, description) values("ArchiveTU4", "ArchiveDescriptionTU");
insert into archive(full_name, description) values("ArchiveTU5", "ArchiveDescriptionTU");
insert into archive(full_name, description) values("ArchiveTU6", "ArchiveDescriptionTU");
insert into archive(full_name, description) values("ArchiveTU7", "ArchiveDescriptionTU");
insert into archive(full_name, description) values("ArchiveTU8", "ArchiveDescriptionTU");
insert into archive(full_name, description) values("ArchiveTU9", "ArchiveDescriptionTU");
insert into archive(full_name, description) values("ArchiveTU10", "ArchiveDescriptionTU");

insert into image(name, description, link, archive_id) values("ImageTU1", "ImageDescriptionTU", "https://TU1.png", 1);
insert into image(name, description, link, archive_id) values("ImageTU2", "ImageDescriptionTU", "https://TU2.png", 1);
insert into image(name, description, link, archive_id) values("ImageTU3", "ImageDescriptionTU", "https://TU3.png", 1);
insert into image(name, description, link, archive_id) values("ImageTU4", "ImageDescriptionTU", "https://TU4.png", 1);
insert into image(name, description, link, archive_id) values("ImageTU5", "ImageDescriptionTU", "https://TU5.png", 1);
insert into image(name, description, link, archive_id) values("ImageTU6", "ImageDescriptionTU", "https://TU6.png", 1);
insert into image(name, description, link, archive_id) values("ImageTU7", "ImageDescriptionTU", "https://TU7.png", 1);
insert into image(name, description, link, archive_id) values("ImageTU8", "ImageDescriptionTU", "https://TU8.png", 1);
insert into image(name, description, link, archive_id) values("ImageTU9", "ImageDescriptionTU", "https://TU9.png", 1);
insert into image(name, description, link, archive_id) values("ImageTU10", "ImageDescriptionTU", "https://TU10.png", 1);

-- COMMITING --
commit;